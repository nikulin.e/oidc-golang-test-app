package main

import (
	"crypto/tls"
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"

	"github.com/caos/oidc/pkg/client/rp"
	"github.com/caos/oidc/pkg/oidc"
	"github.com/caos/oidc/pkg/utils"
)

var (
	callbackPath string = "/auth/callback"
	key          []byte = []byte("test1234test1234")
	provider     rp.RelyingParty
	err          error
	cookName     string = "test_site_jwt"
)

func State() string {
	return uuid.New().String()
}

func CheckSession(w http.ResponseWriter, r *http.Request) string {
	session_cook, err := r.Cookie(cookName)
	if err == nil && session_cook != nil {
		return session_cook.Value
	}
	rp.AuthURLHandler(State, provider)(w, r)
	return ""

}

func RootHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(`
	<html>
		<body>
			<h1>Test Site</h1>
			<a href='/user_info'>Open secure page</a>
		</body>
	</html>
	`))
}

func UserInfoHandler(w http.ResponseWriter, r *http.Request) {
	jwt := CheckSession(w, r)
	if jwt == "" {
		return
	}
	claims := oidc.EmptyIDTokenClaims()
	payload, _ := oidc.ParseToken(jwt, claims)

	content := fmt.Sprintf(`
	<html>
		<body>
			<h1>Test Site</h1>
			<b>User info:</b>
			<p>%s</p>
			<b>JWT:</b>
			<p>%s</p>
		</body>
	</html>
	`, payload, jwt)
	w.Write([]byte(content))
}

func CallbackHandler(w http.ResponseWriter, r *http.Request, tokens *oidc.Tokens, state string, rp rp.RelyingParty, info oidc.UserInfo) {
	cook := http.Cookie{
		Name:     cookName,
		Value:    tokens.IDToken,
		Expires:  time.Now().Add(time.Duration(20 * time.Second)),
		Secure:   false,
		HttpOnly: false,
		Path:     "/",
	}
	http.SetCookie(w, &cook)
	//sessions[state] = info
	http.Redirect(w, r, "/user_info", http.StatusFound) // TODO: url from param
}

func main() {
	clientID := os.Getenv("CLIENT_ID")
	clientSecret := os.Getenv("CLIENT_SECRET")
	keyPath := os.Getenv("KEY_PATH")
	issuer := os.Getenv("ISSUER")
	addr := os.Getenv("ADDR")
	port := os.Getenv("PORT")
	scopes := strings.Split(os.Getenv("SCOPES"), " ")

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}

	redirectURI := fmt.Sprintf("http://%v:%v%v", addr, port, callbackPath)
	cookieHandler := utils.NewCookieHandler(key, key, utils.WithUnsecure())

	options := []rp.Option{
		rp.WithCookieHandler(cookieHandler),
		rp.WithVerifierOpts(rp.WithIssuedAtOffset(5 * time.Second)),
		rp.WithHTTPClient(client),
	}
	if clientSecret == "" {
		options = append(options, rp.WithPKCE(cookieHandler))
	}
	if keyPath != "" {
		options = append(options, rp.WithClientKey(keyPath))
	}

	provider, err = rp.NewRelyingPartyOIDC(issuer, clientID, clientSecret, redirectURI, scopes, options...)
	if err != nil {
		logrus.Fatalf("error creating provider %s", err.Error())
	}

	http.HandleFunc("/", RootHandler)
	http.HandleFunc("/user_info", UserInfoHandler)
	http.Handle(callbackPath, rp.CodeExchangeHandler(rp.UserinfoCallback(CallbackHandler), provider))

	lis := fmt.Sprintf("%s:%s", addr, port)
	logrus.Infof("listening on http://%s/", lis)
	logrus.Fatal(http.ListenAndServe(lis, nil))
}
