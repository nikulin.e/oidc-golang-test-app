FROM golang:1.17-alpine3.14 as builder

WORKDIR /build

# Deps
COPY go.mod go.sum /build/
RUN go mod download

# Build
COPY main.go /build/
RUN go build -o /tmp/oidc-golamg-test-app ./main.go


FROM alpine:3.14 as prod

COPY --from=builder /tmp/oidc-golamg-test-app /opt/oidc-golamg-test-app

ENV \
    CLIENT_ID="any_id" \
    CLIENT_SECRET="any_secret" \
    ISSUER="https://issuer_url" \
    ADDR="127.0.0.1" \
    PORT="9058" \
    SCOPES="openid profile email address phone"

CMD "/opt/oidc-golamg-test-app"
