module gitlab.com/nikulin.e/oidc-golang-test-app

go 1.17

require (
	github.com/caos/oidc v0.15.11
	github.com/google/uuid v1.3.0
	github.com/sirupsen/logrus v1.8.1
)
