package main

import (
	"context"
	"crypto/tls"
	"github.com/caos/oidc/pkg/client"
	"github.com/caos/oidc/pkg/client/rp"
	"github.com/caos/oidc/pkg/oidc"
	log "github.com/sirupsen/logrus"
	"net/http"
	"os"
)

func main() {
	token := "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJPdU9EWmhaM3Z0eFlvelF2SXJYTk4tZzcxQndvODJ0bzBqMzVhTHNfREVNIn0.eyJleHAiOjE2MzUwNzAxOTIsImlhdCI6MTYzNTA2OTg5MiwiYXV0aF90aW1lIjoxNjM1MDY5ODkxLCJqdGkiOiJhZTNmMDViYy0wOWMxLTQ3ODktYTQ4Yi00YzU5Y2ZiYjM4NjIiLCJpc3MiOiJodHRwczovLzUuMjAwLjYwLjI0NTo5MDYwL2F1dGgvcmVhbG1zL3Byb2plY3RhIiwiYXVkIjoiZ2Vva2Fza2FkIiwic3ViIjoiMTU4OTVjYWEtZGQ2Yi00MWYyLWFkZjAtYTliMTMzMmNmMjRmIiwidHlwIjoiSUQiLCJhenAiOiJnZW9rYXNrYWQiLCJzZXNzaW9uX3N0YXRlIjoiMDBhOTY2ZmYtM2Q5Zi00ZGU3LWEzMzctOWYzMDBhY2UyNTc4IiwiYXRfaGFzaCI6IjRNMURSc0NYbUpZd2VLV3VZOHB6N3ciLCJhY3IiOiIxIiwic2lkIjoiMDBhOTY2ZmYtM2Q5Zi00ZGU3LWEzMzctOWYzMDBhY2UyNTc4IiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImFkZHJlc3MiOnt9LCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJ0YWdhbnJvZyJ9.pj1rf0olu74v4UKT5-RzcXElbiU6NTMGAsCqNvUj7622JKsip08DuZpU5YlurOF2_6Qar00f9NUKtwLDsmDoPikD9BQuyzRXJFeSQz2VM48F1HkxEoAGyYul3P_bYD3UFodnS0XkK3JXfjEM8QC4c99yuUgugW0E_tpCUfpGSxZlClbxbpCxJ36bS2pFdw7DxxFeFpGYUa8I1yJIHO9pPr-gwI084_7g3asJT53quiu7IEhooFWbS3hlLViscMJq4ky6230obUlTmJSvuGuSH7dDAUFiTB2RAXvUnxVEZ0f9nx2EL16qxzPtDX668aQxpgXmkZaFVVDRdD8dxueBkg"
	issuer := "https://5.200.60.245:9060/auth/realms/projecta"
	claims := oidc.EmptyIDTokenClaims()

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	httpClient := &http.Client{Transport: tr}

	discoveryConfiguration, err := client.Discover(issuer, httpClient)
	if err != nil {
		log.Error(err)
		os.Exit(-1)
	}
	keySet := rp.NewRemoteKeySet(httpClient, discoveryConfiguration.JwksURI)

	decrypted, err := oidc.DecryptToken(token)
	if err != nil {
		log.Error(err)
		os.Exit(-1)
	}
	payload, err := oidc.ParseToken(decrypted, claims)
	if err != nil {
		log.Error(err)
		os.Exit(-1)
	}
	log.Println(string(payload))

	if err := oidc.CheckSubject(claims); err != nil {
		log.Error(err)
		os.Exit(-1)
	}

	if err = oidc.CheckIssuer(claims, issuer); err != nil {
		log.Error(err)
		os.Exit(-1)
	}

	ctx := context.Background()
	if err = oidc.CheckSignature(ctx, decrypted, payload, claims, []string{}, keySet); err != nil {
		log.Error(err)
		os.Exit(-1)
	}

	if err = oidc.CheckExpiration(claims, 0); err != nil {
		log.Error(err)
		os.Exit(-1)
	}
}
